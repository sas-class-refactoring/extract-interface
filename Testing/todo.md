test cases
- [ ] Simple. Class with one method. No parent. Extract Interface with that method.
- [ ] Simple. Class with one method. No parent. Extract Abstract Parent Class with that method.
- [ ] Simple. Class with one method. Parent Class - no methods. Extract Interface with that method.
- [ ] Simple. Class with one method. Parent Class - no methods. Extract Abstract Parent Class with that method.

