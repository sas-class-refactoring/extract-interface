import pytest
import shutil
from pathlib import Path
import subprocess
import pyautogui
import os
from time import sleep

pyautogui.PAUSE=1
def open_in_labview(file_path):
    # Path to the LabVIEW executable
    labview_exe = r"C:\Program Files\National Instruments\LabVIEW 2020\LabVIEW.exe"

    # Ensure the file exists
    if not os.path.exists(file_path):
        raise FileNotFoundError(f"The file {file_path} does not exist")

    # Launch LabVIEW with the file
    try:
        subprocess.Popen([labview_exe, file_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except Exception as e:
        print(f"Failed to open file in LabVIEW: {e}")

@pytest.fixture
def copy_dir(request, tmp_path):
    """
    Fixture to copy a directory to a temporary location.
    The source directory is passed as a parameter via the request.
    """
    # Get the source directory passed via parametrize
    src_dir = Path(request.param)

    # Destination directory in the temporary location
    dest_dir = tmp_path / src_dir.name
    shutil.copytree(src_dir, dest_dir)

    # Return the path to the copied directory
    return dest_dir

# Sample test case using the fixture
@pytest.mark.parametrize("copy_dir", [
    "StartingPoints/Simple_OneMethod_NoParent/"
], indirect=True)
def test_SimpleCase(copy_dir):
    """
    Test that uses the fixture with different directories.
    """
    # The test will receive the path to the copied directoryi
    file_to_open = Path.joinpath(copy_dir, "OneMethod_NoParent.lvproj")
    open_in_labview(file_to_open)
    sleep(10)
    pyautogui.getWindowsWithTitle("OneMethod_NoParent.lvproj - Project Explorer")[0].maximize()
    sleep(1)
    pyautogui.rightClick(pyautogui.locateCenterOnScreen("images/class OneMethod_NoParent.png"))
    sleep(1)
    pyautogui.moveTo(pyautogui.locateCenterOnScreen("images/menu SAS Class Refactoring.png"))
    sleep(1)
    pyautogui.rightClick(pyautogui.locateCenterOnScreen("images/menu Extract Interface.png"))
    sleep(1)
    pyautogui.getWindowsWithTitle("SASClassRefactoring_Private_Extract.vi")[0].close()